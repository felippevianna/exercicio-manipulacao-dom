const button = document.querySelector(".button");

button.addEventListener('click', (element) => {
    const divPai = document.createElement("div")
    divPai.classList.add("evento")

    const mensagem = document.createElement("p")
    mensagem.classList.add("estilo")

    const caixaDeTexto = document.querySelector("textarea")
    const texto = caixaDeTexto.value
    mensagem.innerText = texto

    const blocotexto = document.createElement("div")
    blocotexto.classList.add("mensagem")
    blocotexto.appendChild(mensagem)
    
    divPai.appendChild(blocotexto)

    const historico = document.querySelector(".historico")
    historico.appendChild(divPai)

    const delButton = document.createElement("button")
    delButton.classList.add("deleta")
    delButton.innerText = "Deletar"
    divPai.appendChild(delButton)

    caixaDeTexto.value = ""


    delButton.addEventListener("click", (element) => {
        event.target.parentNode.remove()
    })

    const editButton = document.createElement("button")
    editButton.classList.add("edita")
    editButton.innerText = "Editar"
    divPai.appendChild(editButton)

    edita(divPai)
    
})

function edita(pai){
    const editaButton = pai.querySelector(".edita")
    

    editaButton.addEventListener("click", (element)=>{
        const pai = element.target.parentNode
        const irma = pai.firstElementChild
        const mensagemParaModificar = irma.firstElementChild.innerText

        const novaCaixa = document.querySelector("textarea")
        novaCaixa.value = mensagemParaModificar

        const buttonManterEdit = document.createElement("button")
        buttonManterEdit.classList.add("altera")
        buttonManterEdit.innerText = "Manter"

        const sessaoEntrada = document.querySelector(".digita")
        sessaoEntrada.appendChild(buttonManterEdit)

        manterAlteracoes(irma, novaCaixa)

    })

}


function manterAlteracoes(divIr, tArea) {
    const buttonManter = document.querySelector(".altera")

    buttonManter.addEventListener("click", (element) => {
        element.target.remove()
        divIr.firstElementChild.innerText = tArea.value
        tArea.value = ""
    })
}

